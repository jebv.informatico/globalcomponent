import { Injectable } from '@angular/core';

// HTTP
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

// Interface
import {IProduct} from './dataApi/interfaces/product.interface'
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable()
export class AppServiceService {

  private url: string = 'api/products';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.url).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(() => new Error(error.message));
      })
    );
  }
}
