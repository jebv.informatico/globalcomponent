import { Component, OnInit } from '@angular/core';

// Service
import { AppServiceService } from './app-service.service';

// Interface
import { IProduct } from './dataApi/interfaces/product.interface';

// Table Parts
import { ITableFields, ITableSettings } from './table/interfaces/table-options.interface';
import { TableService } from './table/table.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  products: IProduct[] = [];

  tableSettings: Partial<ITableSettings>= {
    allowSort: false,
    showOptionsColumn: true,
    showSelectionColumn: true,
    allowMultipleSelection: true,
// paginationSize: [10, 25, 50]
  }

  tableOptions: ITableFields[] = [
    {
      field: 'id',
      filter: false,
      label: 'Identificador',
      position: 'left',
      sort: false,
      type: 'string',
      width: 10
    },
    {
      field: 'name',
      filter: false,
      label: 'Nombre',
      position: 'left',
      sort: false,
      type: 'string',
      width: 10
    },
    {
      field: 'description',
      filter: false,
      label: 'Descripcion',
      position: 'left',
      sort: true,
      type: 'string',
      width: 10
    },
    {
      field: 'price',
      filter: false,
      label: 'Precio',
      position: 'left',
      sort: false,
      type: 'string',
      width: 10
    }
  ]

  constructor(private dataService: AppServiceService, private tableService: TableService) {}

  ngOnInit(): void {
    this.dataService.getProducts().subscribe((data) => {
      this.products = data;
      this.tableService.reloadTable();
    });
  }

  createTest(){
    console.log("LOL")
    console.log(this.tableService.getSelection().selected)
  }
}
