import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { DataService } from './dataApi/data.service';
import { AppServiceService } from './app-service.service';
import { TableService } from './table/table.service';

// HTTP
import { HttpClientModule } from '@angular/common/http';

// Table Component
import { TableModule } from './table/table.module';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    HttpClientInMemoryWebApiModule.forRoot(DataService),
  ],
  providers: [AppServiceService, TableService],
  bootstrap: [AppComponent],
})
export class AppModule {}
