import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';

// CDK
import { SelectionModel } from '@angular/cdk/collections';

// Material
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

// Interface
import {
  ITableFields,
  ITableSettings,
} from './interfaces/table-options.interface';

// Service
import { TableService } from './table.service';
import { Subject, takeUntil } from 'rxjs';

// DefaultSettings
import { defaultSettings } from './default-values/defauld-settings';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass'],
})
export class TableComponent implements AfterViewInit, OnDestroy, OnInit {
  // DATA for component
  @Input() data!: any;
  // @Input() dataService! : any;
  // @Input() showOptions: boolean = false;
  // @Input() selection: boolean = false;
  // @Input() multiSelection: boolean = true;
  // @Input() sortOption: boolean = true;
  @Input() tableSettings!: Partial<ITableSettings>;
  @Input() tableFields: ITableFields[] = [];

  // Functions on options
  @Input() editFunction!: (id: string) => void;
  @Input() deleteFunction!: (id: string) => void;
  @Input() createFunction!: () => void;

  // Table parts
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  // Unsub
  unSub$: Subject<void> = new Subject();

  // DisplayColumns
  displayColumn: string[] = [];
  filterColumn: string[] = [];

  // Default Settings
  defaultSettings = defaultSettings;

  // Filter
  filterValues: any = {};

  // Function OUTPUT
  // @Output() editOutPut = new EventEmitter<string>();
  // @Output() deleteOutPut = new EventEmitter<string>();

  // Table variables
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);

  constructor(public tableService: TableService) {}

  ngOnInit(): void {
    this.defaultSettings = this.updateDefaultOptions(
      this.defaultSettings,
      this.tableSettings
    );
    this.createHeaderLabel();
  }

  ngOnDestroy(): void {
    this.unSub$.next();
    this.unSub$.complete();
  }

  updateDefaultOptions(
    defaultOp: ITableSettings,
    userOp: Partial<ITableSettings>
  ) {
    return { ...defaultOp, ...userOp };
  }

  ngAfterViewInit(): void {
    this.tableService.reloadTableVar
      .pipe(takeUntil(this.unSub$))
      .subscribe((reload) => {
        if (reload) {
          this.dataSource.data = this.data;
          console.log(this.dataSource.data);
          this.tableService.reloadTableVar.next(false);
        }
      });

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = this.createCustomFilter();
  }

  createHeaderLabel() {

    this.displayColumn = this.tableFields.map((options) => options.field);

    this.defaultSettings.showOptionsColumn &&
      this.displayColumn.push('options');

    this.defaultSettings.showSelectionColumn &&
      this.defaultSettings.allowMultipleSelection &&
      this.displayColumn.unshift('select');
  }

  // Filter

  // createCustomFilter(column: string) {
  //   this.dataSource.filterPredicate = (source: any, filterValue: string) => {
  //     let valueFromSource = (source[column] && source[column]) || null;

  //     const searchValues = JSON.parse(filterValue);

  //     let haveFilterValues = false;

  //     let finalValue = false;

  //     for (const key in searchValues) {
  //       if (Object.prototype.hasOwnProperty.call(searchValues, key)) {
  //         if (searchValues[key] !== '') {
  //           haveFilterValues = true;
  //         } else {
  //           delete searchValues[key];
  //         }
  //       }
  //     }

  //     if (haveFilterValues) {
  //       for (const key in searchValues) {
  //         if (Object.prototype.hasOwnProperty.call(searchValues, key)) {
  //           valueFromSource = source[key] && source[key];

  //           if (
  //             (valueFromSource as string)
  //               .trim()
  //               .toLocaleLowerCase()
  //               .indexOf(searchValues[key]) !== -1
  //           ) {
  //             finalValue = true;
  //           } else {
  //             finalValue = false;
  //           }
  //         }
  //       }
  //       return finalValue;
  //     } else return true;
  //   };
  // }

  createCustomFilter() {
    return (source: any, filterValue: string) => {
      let valueFromSource: string = '';

      const searchValues = JSON.parse(filterValue);

      let haveFilterValues = false;

      let finalValue = false;

      for (const key in searchValues) {
        if (Object.prototype.hasOwnProperty.call(searchValues, key)) {
          if (searchValues[key] !== '') {
            haveFilterValues = true;
          } else {
            delete searchValues[key];
          }
        }
      }

      if (haveFilterValues) {
        for (const key in searchValues) {
          if (Object.prototype.hasOwnProperty.call(searchValues, key)) {
            valueFromSource = source[key] && source[key];

            if (
              (valueFromSource as string)
                .trim()
                .toLocaleLowerCase()
                .indexOf(searchValues[key]) !== -1
            ) {
              finalValue = true;
            } else {
              finalValue = false;
            }
          }
        }
        return finalValue;
      } else return true;
    };
  }

  executeFilter(event: any, field: string) {
    const value = (event as HTMLInputElement).value; // GET THE VALUE

    this.filterValues[field] = value.trim().toLowerCase(); // CREATE A FILTER OBJECT

    this.dataSource.filter = JSON.stringify(this.filterValues); // CONVERT THE OBJECT TO JSON

    // In case the value is not empty string
    if (value !== '') this.dataSource.data = this.dataSource.filteredData; // Asign to the dataSource the filtered data
    else {
      this.dataSource.data = this.data; // Return to normality
    }
  }

  // End filter

  disableSort(mainCriteria: boolean, secondCriteria: boolean) {
    return mainCriteria ? !secondCriteria : true;
  }

  // SELECT METHODS

  selectRow(row: any): void {
    if (this.defaultSettings.showSelectionColumn) {
      if (!this.defaultSettings.allowMultipleSelection) {
        this.tableService.getSelection().clear();
      }
      this.tableService.selectionData.toggle(row);
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.tableService.selectionData.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.tableService.selectionData.clear();
      return;
    }
    this.tableService.selectionData.select(...this.dataSource.data);
  }

  // /** The label for the checkbox on the passed row */
  // checkboxLabel(row?: any): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
  //   }
  //   return `${this.tableService.selectionData.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  // }
}
