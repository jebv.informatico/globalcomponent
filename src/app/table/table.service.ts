import { SelectionModel } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class TableService {

  selectionData = new SelectionModel<any>(true, []);

  reloadTableVar : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { 
    
  }

  getSelection() {
    return this.selectionData;
  }

  reloadTable(){
    this.reloadTableVar.next(true);
  }
}
