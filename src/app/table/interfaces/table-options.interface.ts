export interface ITableFields {
  label: string;
  field: string;
  width: number;
  position: 'right' | 'left' | 'center';
  sort: boolean;
  filter: boolean;
  type: 'string' | 'number';
}

export interface ITableSettings {
  showOptionsColumn: boolean;
  showSelectionColumn: boolean;
  allowMultipleSelection: boolean;
  allowSort: boolean;
  allowFilters: boolean;
  paginationSize: number[];
  optionsLabel: string;
}
