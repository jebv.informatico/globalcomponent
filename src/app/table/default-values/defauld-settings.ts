import { ITableSettings } from '../interfaces/table-options.interface';

export let defaultSettings: ITableSettings = {
  allowFilters: false,
  allowMultipleSelection: false,
  allowSort: false,
  paginationSize: [5, 10, 15],
  showOptionsColumn: false,
  showSelectionColumn: false,
  optionsLabel: 'Opciones'
};
